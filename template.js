(function () {
  // Assume jQuery is available on the page.
  const $ = window.$;

  /**
   * Should return one of the following types for the current page
   * being viewed:
   *
   *  - `homepage` - When viewing the homepage.
   *  - `product` - When viewing an individual product.
   *  - `category` - When viewing a listing of multiple products.
   *  - `cart` - When viewing the shopping cart.
   *  - `other` - For other pages not matching the above.
   */
  function getPageType () {
    // ...
  }

  /**
   * Should return the currency code of the store.
   * E.g. "EUR" or "USD".
   */
  function getCurrencyCode () {
    // ...
  }

  /**
   * Should parse text containing a price and return the decimal
   * value of the amount. For example:
   *
   * @example
   * parsePrice('$59.00'); // => 59
   * parsePrice('USD 149.50'); // => 149.50
   * parsePrice('€ 1.200,99'); // => 1200.99
   * parsePrice('₹4,099.00'); // => 4099
   * parsePrice('- 10 EUR'); // => -10
   */
  function parsePrice (text) {
    // ...
  }

  /**
   * When on a product page, it should return the product name.
   */
  function getProductName () {
    return '';
  }

  /**
   * When on a product page, it should return the product price.
   *
   *  1. If the product is on sale, the final sale price should be returned.
   *  2. The price returned must be a decimal number (not a string).
   */
  function getProductPrice () {
    // ...
  }

  /**
   * When on a product or category page, it should return an array
   * of breadcrumbs to the current page.
   *
   * @example
   * getBreadcrumbs(); // => ['Clothing', 'Unisex', 'Jeans']
   */
  function getBreadcrumbs () {
    // ...
  }

  /**
   * Should return the cart subtotal. This is the total cost of all
   * items in the cart, before any discount, tax or shipping is added.
   */
  function getCartSubtotal () {
    // ...
  }

  /**
   * Should return the amount of any discount that is currently applied
   * to the cart.
   */
  function getCartDiscount () {
    // ...
  }

  /**
   * Should return the code of any discount that is applied to the cart.
   */
  function getCartDiscountCode () {
    // ...
  }

  /**
   * Should return an array of products that have been added to the
   * shopping cart. Each product in the array should be an object
   * with the following properties:
   *
   *  - name: The product name
   *  - price: The unit price (cost of a single quantity)
   *  - quantity: The number of units of this product in the cart
   *
   * @example
   * getCartProducts();
   * // => [
   *   {
   *     name: "Oversized Sherpa Trucker Jacket",
   *     price: 168,
   *     quantity: 2
   *   }
   * ]
   * // In this case the cart subtotal would be 168 x 2 = 336.00
   */
  function getCartProducts () {
    // ...
  }

  /**
   * Should insert the discount code into the input on the cart page
   * and apply it to the cart.
   */
  function applyDiscountCode (discountCode) {
    // ...
  }

  function trackPage () {
    // Collect all data to be tracked.
    const data = {
      pageType: getPageType(),
      productName: getProductName(),
      productPrice: getProductPrice(),
      breadcrumbs: getBreadcrumbs(),
      currencyCode: getCurrencyCode(),
      cartSubtotal: getCartSubtotal(),
      cartDiscount: getCartDiscount(),
      cartDiscountCode: getCartDiscountCode(),
      cartProducts: getCartProducts()
    };
    // Convert all `undefined` values to `null` to prevent them
    // from being omitted.
    for (const key of Object.keys(data)) {
      if (data[key] === undefined) {
        data[key] = null;
      }
    }
    // Log data to be tracked.
    console.log('trackPage:', JSON.stringify(data, null, 2));
  }

  /**
   * This is the main entry point of the script. It should call the
   * `trackPage` method in the following scenarios when the page loads and is
   * ready for the "get.." methods you implemented above to be executed.
   */
  function main () {
    // Track the initial state when the page loads.
    trackPage();
  }

  // Expose some methods.
  window.adaptor = {
    main,
    trackPage,
    applyDiscountCode
  };

  main();
}());

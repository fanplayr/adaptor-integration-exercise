# JavaScript Integration Exercise

Fanplayr relies on capturing ecommerce data to feed into its realtime
segmentation engine.

One of the ways we integrate our platform into an ecommerce website is to
provide a client with a single JavaScript file tailored to their site. The
script is included on all pages of their site and is responsible for capturing
the data Fanplayr requires by extracting it from the DOM, data layers and other
means that are specific to the site.

The goal of this exercise is to understand the common types of data Fanplayr
relies on and some of the challenges we face when building this type of
integration.

## Instructions

We'll use the Levi store as an example ecommerce site to integrate into.
*(Note: At the time of writing Fanplayr has no affiliation with this brand or
website)*

1. Using Google Chrome, navigate to https://www.levi.com.

2. Open the Developer Tools and navigate to the **Snippets** area.

    _See https://developers.google.com/web/tools/chrome-devtools/snippets for
    information on using snippets if you're not familiar with them._

3. Create a new snippet and start by pasting the contents of `template.js` into
  the editor.

    _If you run the snippet you should see a data object printed in the
    console._

4. Your goal is to read through the comments in the template and implement the
  functions provided to extract the data from the website when the script is run.

## Validation

The site is split into multiple stores and languages. Your implementation should
work for **at least** the **United States** and **Italian** stores. _(Bonus if it also
works for the India store!)_

To help validate your implementation, we've included a list of sample urls and
their expected output below.

_Note: The website may have changed since the time of writing this exercise and
only the output relevant to each page type has been included._

---

https://www.levi.com/IT/it_IT/

```js
{
  "pageType": "homepage",
  "currencyCode": "EUR"
}
```

---

https://www.levi.com/US/en_US/

```js
{
  "pageType": "homepage",
  "currencyCode": "USD"
}
```

---

https://www.levi.com/IT/it_IT/abbigliamento/uomo/jeans/c/levi_clothing_men_jeans

```js
{
  "pageType": "category",
  "breadcrumbs": [
    "Abbigliamento",
    "Uomo",
    "Jeans"
  ],
  "currencyCode": "EUR",
}
```

---

https://www.levi.com/US/en_US/apparel/clothing/bottoms/721-high-rise-skinny-jeans/p/188820120

*Note: At the time of writing, this product was on sale for $29.98. It's normal
price was $59.50.*

```js
{
  "pageType": "product",
  "productName": "721 High Rise Skinny Jeans",
  "productPrice": 29.98,
  "breadcrumbs": [
    "Apparel",
    "Clothing",
    "Bottoms",
    "721 High Rise Skinny Jeans"
  ],
  "currencyCode": "USD"
}
```

---

https://www.levi.com/US/en_US/cart

The state of the cart page after applying discount code `VISA25` and adding
the following products:

- 1 of https://www.levi.com/US/en_US/clothing/women/outerwear/oversized-sherpa-trucker-jacket/p/578960000
- 2 of https://www.levi.com/US/en_US/clothing/men/jeans/501-original-fit-jeans/p/005010193

```js
{
  "pageType": "cart",
  "currencyCode": "USD",
  "cartSubtotal": 287,
  "cartDiscount": 71.75,
  "cartDiscountCode": "VISA25",
  "cartProducts": [
    {
      "name": "Oversized Sherpa Trucker Jacket",
      "price": 168,
      "quantity": 1
    },
    {
      "name": "501® Original Fit Jeans",
      "price": 59.5,
      "quantity": 2
    }
  ]
}
```
